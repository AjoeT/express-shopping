import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


public class testShopping {

    @Test
    public void shouldBeAbleToCreateShoppingCart() {
        Item earphones = new Item("Earphones", 300, ItemCategory.Electronics);
        assertNotNull(new EShop(earphones, new Courier(), DeliveryPlace.Bangalore));
    }

    @Test
    public void shouldBeAbleToCreateItem() {
        Item earphones = new Item("Earphones", 300, ItemCategory.Electronics);
        assertNotNull(earphones);
    }

    @Test
    public void shouldBeAbleToCreateCourier() {
        assertNotNull(new Courier());
    }


    @Test
    public void shouldBeAbleToGetCategorySpecificChargeForBlueDart() {
        Item earphones = new Item("Earphones", 300, ItemCategory.Electronics);
        BlueDart blueDart = new BlueDart();
        assertEquals(60, blueDart.getCategorySpecificCharge(earphones), 0);
    }

    @Test
    public void shouldBeAbleToGetCategorySpecificChargeAndCitySpecificChargesForBlueDart() {
        Item iron_box = new Item("Iron Box", 1200, ItemCategory.Home_Appliances);
        BlueDart blueDart = new BlueDart();
        assertEquals(0, blueDart.getCategorySpecificCharge(iron_box), 0);
        assertEquals(60, blueDart.getCitySpecificCharge(DeliveryPlace.Bangalore), 0);
    }


    @Test
    public void shouldBeAbleToGetCategorySpecificChargeForFedEx() {
        Item earphones = new Item("Earphones", 300, ItemCategory.Electronics);
        FedEx fedEx = new FedEx();
        assertEquals(140, fedEx.getCategorySpecificCharge(earphones), 0);
    }

    @Test
    public void shouldBeAbleToGetCategorySpecificChargeAndCitySpecificChargesForFedEx() {
        Item shoes = new Item("Shoes", 700, ItemCategory.Fitness);
        FedEx fedEx = new FedEx();
        assertEquals(70, fedEx.getCategorySpecificCharge(shoes), 0);
        assertEquals(154, fedEx.getCitySpecificCharge(DeliveryPlace.Chennai), 0);
    }


    @Test
    public void shouldBeAbleToGetCitySpecificCharge() {
        BlueDart blueDart = new BlueDart();
        assertEquals(0, blueDart.getCitySpecificCharge(DeliveryPlace.Chennai), 0);
    }

    @Test
    public void shouldBeAbleToBuyIronBoxFromBlueDartDeliveredToBangalore() {
        Item iron_box = new Item("Iron Box", 1200, ItemCategory.Home_Appliances);
        BlueDart blueDart = new BlueDart();
        assertEquals(1260, new EShop(iron_box, blueDart, DeliveryPlace.Bangalore).buy(), 0);
    }

    @Test
    public void shouldBeAbleToBuyShoesFromFedExDeliveredToChennai() {
        Item shoes = new Item("Shoes", 700, ItemCategory.Fitness);
        FedEx fedEx = new FedEx();
        assertEquals(924, new EShop(shoes, fedEx, DeliveryPlace.Chennai).buy(), 0);
    }

    @Test
    public void shouldBeAbleToGetLeastPriceCourierForOneItem() {
        List<Courier> courierList = new ArrayList<>();
        Item shoes = new Item("Shoes", 300, ItemCategory.Fitness);


        BlueDart blueDart = new BlueDart();
        FedEx fedEx = new FedEx();
        courierList.add(blueDart);
        courierList.add(fedEx);
        System.out.println("GetLeastPriceCourierForOneItem");
        System.out.println("######");
        assertEquals("CourierName : BlueDart LeastPrice :330.0", new EShop(shoes, courierList, DeliveryPlace.Chennai).leastCost());
        System.out.println("######");
    }

    @Test
    public void shouldBeAbleToGetLeastPriceCourierForListOfItem() {
        List<Courier> courierList = new ArrayList<>();
        List<Item> itemList = new ArrayList<>();
        List<DeliveryPlace> deliveryPlaceList = new ArrayList<>();

        BlueDart blueDart = new BlueDart();
        FedEx fedEx = new FedEx();
        courierList.add(blueDart);
        courierList.add(fedEx);


        Item shoes = new Item("shoes", 300, ItemCategory.Electronics);
        Item iron_box = new Item("Iron Box", 300, ItemCategory.Electronics);
        itemList.add(shoes);
        itemList.add(iron_box);


        deliveryPlaceList.add(DeliveryPlace.Chennai);
        deliveryPlaceList.add(DeliveryPlace.Bangalore);

        System.out.println("\n\n");
        System.out.println("GetLeastPriceCourierForListOfItem");
        System.out.println("######");
        assertEquals("CourierName : BlueDart LeastPrice :774.0", new EShop(itemList, courierList, deliveryPlaceList).
                leastCostForDeliveryGivenCourierListWithDeliveryPlaceList());
        System.out.println("######");
        ;
    }



}
