public interface CourierWithCategorySpecificCharge {
    public float getCategorySpecificCharge(Item item);
}
