public class BlueDart extends Courier implements CourierWithCategorySpecificCharge, CourierWithCitySpecificCharge {

    private String name = "BlueDart";
    private float electronicsExtraDeliveryChargePercentage = 0.2f;
    private float noExtraCharges = 0;
    private float itemPriceAddedToCategorySpecificCharge = 0;
    private float citySpecificChargesForChennai = 0.1f;
    private float citySpecificChargesForBangalore = 0.05f;


    public String getCourierName() {
        return name;
    }


    @Override
    public float getCategorySpecificCharge(Item item) {
        ItemCategory itemCategory = item.getItemCategory();
        if (itemCategory.equals(ItemCategory.Electronics)) {
            itemPriceAddedToCategorySpecificCharge = item.price + item.price * electronicsExtraDeliveryChargePercentage;
            return item.price * electronicsExtraDeliveryChargePercentage;
        }
        itemPriceAddedToCategorySpecificCharge = item.price + noExtraCharges;
        return 0;
    }

    @Override
    public float getCitySpecificCharge(DeliveryPlace deliveryPlace) {
        if (deliveryPlace.equals(DeliveryPlace.Chennai))
            return itemPriceAddedToCategorySpecificCharge * citySpecificChargesForChennai;
        return itemPriceAddedToCategorySpecificCharge * citySpecificChargesForBangalore;

    }
}
