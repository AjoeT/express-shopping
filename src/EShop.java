import java.util.List;

public class EShop {
    Item item;
    Courier courier;
    List<Courier> courierList;
    DeliveryPlace deliveryPlace;
    List<Item> itemList;
    List<DeliveryPlace> deliveryPlaceList;


    EShop(Item item, Courier courier, DeliveryPlace deliveryPlace) {
        this.item = item;
        this.courier = courier;
        this.deliveryPlace = deliveryPlace;
    }

    EShop(Item item, List<Courier> courierList, DeliveryPlace deliveryPlace) {
        this.item = item;
        this.courierList = courierList;
        this.deliveryPlace = deliveryPlace;
    }

    EShop(List<Item> itemList, List<Courier> courierList, List<DeliveryPlace> deliveryPlaceList) {
        this.itemList = itemList;
        this.courierList = courierList;
        this.deliveryPlaceList = deliveryPlaceList;
    }

    public float buy() {
        return item.price + courier.getCategorySpecificCharge(item) + courier.getCitySpecificCharge(deliveryPlace);

    }

    public String leastCost() {
        String CourierName = "";
        float leastPrice = 0;
        for (Courier courier : courierList) {
            float tempLeastPrice = item.price + courier.getCategorySpecificCharge(item) + courier.getCitySpecificCharge(deliveryPlace);
            System.out.println(courier.getCourierName());
            System.out.println(item.price + " " + courier.getCategorySpecificCharge(item)
                    + " " + courier.getCitySpecificCharge(deliveryPlace) + " " + tempLeastPrice);
            if (leastPrice == 0) {
                leastPrice = tempLeastPrice;
            }
            if (tempLeastPrice <= leastPrice) {
                CourierName = courier.getCourierName();
                leastPrice = tempLeastPrice;
            }

        }
        System.out.println("Least price is " + leastPrice + " provided by " + CourierName);
        return "CourierName : " + CourierName + " LeastPrice :" + leastPrice;
    }

    public String leastCostForDeliveryGivenCourierListWithDeliveryPlaceList() {
        String CourierName = "";
        float leastPrice = 0;
        for (Courier courier : courierList) {
            float tempLeastPriceForCourier = 0;
            for (int iterator = 0 ;iterator < itemList.size();iterator++) {
                Item item = itemList.get(iterator);
                DeliveryPlace deliveryPlace =deliveryPlaceList.get(iterator);
                float tempLeastPrice = item.price + courier.getCategorySpecificCharge(item) + courier.getCitySpecificCharge(deliveryPlace);
                tempLeastPriceForCourier += tempLeastPrice;
                System.out.println(courier.getCourierName()+" : "+item.name+" to "+deliveryPlace
                        +" with cost "+tempLeastPrice);
            }

            if (leastPrice == 0) {
                leastPrice = tempLeastPriceForCourier;
            }

            if (tempLeastPriceForCourier <= leastPrice) {
                CourierName = courier.getCourierName();
                leastPrice = tempLeastPriceForCourier;
            }
            System.out.println("Total cost for delivering both product by "+courier.getCourierName() +" : " + leastPrice);

        }
        System.out.println("Least price is " + leastPrice + " provided by " + CourierName);
        return "CourierName : " + CourierName + " LeastPrice :" + leastPrice;
    }
}
