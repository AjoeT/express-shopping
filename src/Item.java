public class Item {

    String name;
    float price;
    ItemCategory itemCategory;

    Item (String name, float price,ItemCategory itemCategory) {
        this.name=name;
        this.price=price;
        this.itemCategory=itemCategory;
    }


    public ItemCategory getItemCategory() {
        return this.itemCategory;
    }


}
