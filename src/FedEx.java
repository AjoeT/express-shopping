public class FedEx extends Courier implements CourierWithCategorySpecificCharge, CourierWithCitySpecificCharge {

    private String name = "FedEx";
    private float electronicsFlatDeliveryCharge = 50;
    private float electronicsExtraDeliveryChargePercentage = 0.3f;
    private float fitnessExtraDeliveryChargePercentage = 0.1f;

    private float itemPriceAddedToCategorySpecificCharge = 0;

    private float citySpecificChargesForChennai = 0.2f;
    private float citySpecificChargesForBangalore = 0.15f;

    public String getCourierName() {
        return name;
    }

    public float getCategorySpecificCharge(Item item) {
        ItemCategory itemCategory = item.getItemCategory();
        if (itemCategory.equals(ItemCategory.Electronics)) {
            itemPriceAddedToCategorySpecificCharge = item.price + electronicsFlatDeliveryCharge
                    + (item.price * electronicsExtraDeliveryChargePercentage);
            return electronicsFlatDeliveryCharge + (item.price * electronicsExtraDeliveryChargePercentage);
        } else
            itemPriceAddedToCategorySpecificCharge = item.price
                    + (item.price * fitnessExtraDeliveryChargePercentage);
            return item.price * fitnessExtraDeliveryChargePercentage;
    }

    @Override
    public float getCitySpecificCharge(DeliveryPlace deliveryPlace) {
        if (deliveryPlace.equals(DeliveryPlace.Chennai))
            return itemPriceAddedToCategorySpecificCharge * citySpecificChargesForChennai;
        return itemPriceAddedToCategorySpecificCharge * citySpecificChargesForBangalore;
    }


}
