public interface CourierWithCitySpecificCharge {
    public float getCitySpecificCharge(DeliveryPlace deliveryPlace);

}
